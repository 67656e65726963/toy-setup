<?php

/**
 * Plugin Name:       Toy Setup
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-setup
 * Description:       Adds sensible theme support tweaks.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action(
	'after_setup_theme',
	function() {
		add_theme_support( 'html5', array( 'gallery', 'caption' ) );
		add_theme_support( 'title-tag' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'nav-menus' );

		add_post_type_support( 'page', array( 'excerpt' ) );

		// Enable shortcodes for these
		foreach ( array( 'term_description', 'the_excerpt', 'comment_text', 'widget_text' ) as $widget ) {
			add_filter( $widget, 'do_shortcode' );
		}

		add_filter( 'use_default_gallery_style', '__return_false' );
	},
	67
);

add_action(
	'wp_head',
	function() {
		echo '<link rel="profile" href="http://gmpg.org/xfn/11">';
		echo '<link rel="pingback" href="' . esc_url( get_bloginfo( 'pingback_url' ) ) . '">';
	},
	9
);

// Remove inline style width
add_filter(
	'img_caption_shortcode_width',
	function() {
		return 0;
	},
	67
);
