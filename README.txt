=== Toy Setup ===
Contributors: thewhodidthis
Tags: util
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Installs sensible theme support tweaks, removes inline styles from shortcode produced images, puts the pingback url in HTML head.
